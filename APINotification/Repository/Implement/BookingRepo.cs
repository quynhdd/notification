﻿using APINotification.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APINotification.Models.Solution_30Shine;
using Microsoft.EntityFrameworkCore;

namespace APINotification.Repository.Implement
{
    public class BookingRepo: IBookingRepo
    {
        private readonly Solution_30shineContext dbContext;
        private readonly DbSet<Booking> dbSet;

        public BookingRepo(Solution_30shineContext dbContext)
        {
            this.dbContext = dbContext;
            dbSet = dbContext.Set<Booking>();
        }

        public async Task<int?> GetOSById(int Id)
        {
            try
            {
                var data = await dbSet.Where(r => r.Id == Id).Select(r => r.Os).FirstOrDefaultAsync();
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
