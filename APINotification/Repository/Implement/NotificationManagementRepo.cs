﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using APINotification.Models.Solution_30Shine;
using APINotification.Repository.Interface;
using Microsoft.EntityFrameworkCore;

namespace APINotification.Repository.Implement
{
    /// <summary>
    /// 
    /// </summary>
    public class NotificationManagementRepo : INotificationManagementRepo
    {
        private readonly Solution_30shineContext dbContext;
        private readonly DbSet<NotificationManagement> dbSet;

        //constructor
        public NotificationManagementRepo(Solution_30shineContext dbContext)
        {
            this.dbContext = dbContext;
            dbSet = dbContext.Set<NotificationManagement>();
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="obj"></param>
        public void Update(NotificationManagement obj)
        {
            try
            {
                dbSet.Update(obj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="obj"></param>
        public void Delete(NotificationManagement obj)
        {
            try
            {
                obj.ModifiedTime = DateTime.Now;
                obj.IsDelete = true;
                dbSet.Update(obj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="obj"></param>
        public void Insert(NotificationManagement obj)
        {
            try
            {
                dbSet.Add(obj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<NotificationManagement> GetById(int id)
        {
            try
            {
                return await dbSet.FirstOrDefaultAsync(r => r.Id == id && r.IsDelete == false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get 
        /// </summary>
        /// <param name="expresstion"></param>
        /// <returns></returns>
        public async Task<NotificationManagement> Get(Expression<Func<NotificationManagement, bool>> expresstion)
        {
            try
            {
                return await dbSet.FirstOrDefaultAsync(expresstion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get list
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public async Task<List<NotificationManagement>> GetList(Expression<Func<NotificationManagement, bool>> expression)
        {
            try
            {
                return await dbSet.Where(expression).ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Check trùng url thông báo
        /// </summary>
        /// <returns></returns>
        public async Task<bool> CheckDuplicateUrl(string url)
        {
            try
            {
                var data = await dbSet.FirstOrDefaultAsync(r => r.IsDelete == false && r.Url == url);
                return data == null ? false : true;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        /// <summary>
        /// Check trùng tên thông báo
        /// </summary>
        /// <returns></returns>
        public async Task<bool> CheckDuplicateName(string name)
        {
            try
            {
                var data = await dbSet.FirstOrDefaultAsync(r => r.IsDelete == false && r.Name == name);
                return data == null ? false : true;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Save change
        /// </summary>
        /// <returns></returns>
        public async Task SaveChangeAsync()
        {
            try
            {
                await dbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
