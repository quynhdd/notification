﻿using APINotification.Models.CustomeModel;
using APINotification.Models.Solution_30Shine;
using APINotification.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace APINotification.Repository.Implement
{
    public class NotificationUsersRepo : INotificationUsersRepo
    {
        private readonly Solution_30shineContext dbContext;
        private readonly DbSet<NotificationUsers> dbSet;
        //constructor
        public NotificationUsersRepo(Solution_30shineContext dbContext)
        {
            this.dbContext = dbContext;
            dbSet = dbContext.Set<NotificationUsers>();
        }

        /// <summary>
        /// Get list Notification
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="notiId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<OutPutModel.Output_Notifications>> Get(int userId, int top, string slugkey)
        {
            try
            {
                List<OutPutModel.Output_Notifications> list = new List<OutPutModel.Output_Notifications>();
                list = (from a in dbContext.NotificationUsers.AsNoTracking()
                        join b in dbContext.NotificationManagement.AsNoTracking() on a.NotiId equals b.Id
                        where a.UserId == userId
                                && b.IsDelete == false
                                && b.IsPublish == true
                                && a.IsDelete == false
                                && a.SlugKey == slugkey
                        orderby b.Id descending
                        select new OutPutModel.Output_Notifications
                        {
                            Id = b.Id,
                            Name = b.Name ?? "",
                            Title = b.Title ?? "",
                            Descriptions = b.Descriptions ?? "",
                            Url = b.Url ?? "",
                            Image = b.Images ?? "",
                            CreatedTime = String.Format("{0:HH:mm dd/MM/yyyy}", a.CreatedTime),
                            Status = a.Status ?? 0,
                            Type = b.Type ?? 0
                        })
                       .Take(top)
                       //.OrderByDescending(a => a.Id)
                       .ToList();

                return list;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Get tổng số notification chưa đọc của nhân viên
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="top"></param>
        /// <returns></returns>
        public async Task<int> GetTotalNotificationNotSeen(int userId, int top, string slugkey)
        {
            try
            {
                var data = (from a in dbContext.NotificationUsers.AsNoTracking()
                            where a.UserId == userId
                                  && a.Status == 1
                                  && a.IsDelete == false
                                  && a.SlugKey == slugkey
                            select new
                            {
                                Id = a.Id,
                                CreatedTime = a.CreatedTime
                            })
                         .Take(top)
                         .OrderByDescending(a => a.CreatedTime)
                         .Count();

                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// get by id
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public async Task<NotificationUsers> GetById(Expression<Func<NotificationUsers, bool>> expression)
        {
            try
            {
                return await dbSet.Where(expression).FirstOrDefaultAsync();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Insert list data
        /// </summary>
        /// <param name="obj"></param>
        public void Insert(IEnumerable<NotificationUsers> obj)
        {
            try
            {
                dbSet.AddRange(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void InsertOnlyRow(NotificationUsers obj)
        {
            try
            {
                dbSet.Add(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Update
        /// </summary>
        public void Update(NotificationUsers obj)
        {
            try
            {
                dbSet.Update(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// save change
        /// </summary>
        /// <returns></returns>
        public async Task SaveChange()
        {
            try
            {
                await dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Get list notification user
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public async Task<IEnumerable<NotificationUsers>> GetList(Expression<Func<NotificationUsers, bool>> expression)
        {
            try
            {
                return await dbSet.Where(expression).ToListAsync();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}
