﻿using APINotification.Models.CustomeModel;
using APINotification.Models.Solution_30Shine;
using APINotification.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static APINotification.Models.CustomeModel.OutPutModel;

namespace APINotification.Repository.Implement
{
    public class StaffTypeRepo : IStaffTypeRepo
    {
        private readonly Solution_30shineContext dbContext;
        //constructor
        public StaffTypeRepo(Solution_30shineContext dbContext)
        {
            this.dbContext = dbContext;
        }

        /// <summary>
        /// Get list Id department
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<OutPutModel.Output_Department>> Get()
        {
            try
            {
                var data = (from c in dbContext.StaffType.AsNoTracking()
                            where c.IsDelete == 0
                                  && c.Publish == true
                            select new Output_Department
                            {
                                Id = c.Id
                            }).ToList();

                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
