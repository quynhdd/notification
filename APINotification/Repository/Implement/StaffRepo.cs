﻿using APINotification.Models.CustomeModel;
using APINotification.Models.Solution_30Shine;
using APINotification.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using static APINotification.Models.CustomeModel.OutPutModel;

namespace APINotification.Repository.Implement
{
    public class StaffRepo : IStaffRepo
    {
        private readonly Solution_30shineContext dbContext;
        private readonly DbSet<Staff> dbSet;
        //constructor
        public StaffRepo(Solution_30shineContext dbContext)
        {
            this.dbContext = dbContext;
            dbSet = dbContext.Set<Staff>();
        }

        /// <summary>
        /// Get list staff
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public IEnumerable<Staff> Get(Expression<Func<Staff, bool>> expression)
        {
            try
            {
                var data = dbSet.Where(expression).ToList();

                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<IEnumerable<Staff>> GetByIds(IEnumerable<int> ids)
        {
            try
            {
                return await dbSet.Where(a => a.IsDelete == 0 && a.Active == 1 && ids.Contains(a.Id)).ToListAsync();
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        /// <summary>
        /// get list staff by Salonid
        /// </summary>
        /// <param name="salonId"></param>
        /// <returns></returns>
        public IEnumerable<OutPutModel.Ouput_Staff> GetList(IEnumerable<int> salonId)
        {
            try
            {
                var data = (from c in dbContext.Staff.AsNoTracking()
                            where c.Active == 1 && c.IsDelete == 0 && salonId.Contains(c.SalonId.Value)
                            select new Ouput_Staff
                            {
                                Id = c.Id,
                                DepartmentId = c.Type ?? 0
                            }).ToList();

                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
