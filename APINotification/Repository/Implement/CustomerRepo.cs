﻿using APINotification.Models.Solution_30Shine;
using APINotification.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APINotification.Repository.Implement
{
    public class CustomerRepo : ICustomerRepo
    {
        private readonly Solution_30shineContext dbContext;
        private readonly DbSet<Customer> dbSet;

        public CustomerRepo(Solution_30shineContext dbContext)
        {
            this.dbContext = dbContext;
            dbSet = dbContext.Set<Customer>();
        }

        public async Task<IEnumerable<Customer>> GetByListIds(IEnumerable<int> ids)
        {
            try
            {
                return await dbSet.Where(a => a.IsDelete == 0 && ids.Contains(a.Id)).ToListAsync();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
