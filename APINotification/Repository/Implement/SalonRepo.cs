﻿using APINotification.Models.CustomeModel;
using APINotification.Models.Solution_30Shine;
using APINotification.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using static APINotification.Models.CustomeModel.OutPutModel;

namespace APINotification.Repository.Implement
{
    public class SalonRepo : ISalonRepo
    {
        private readonly Solution_30shineContext dbContext;
        //constructor
        public SalonRepo(Solution_30shineContext dbContext)
        {
            this.dbContext = dbContext;
        }

        /// <summary>
        /// Get list Id Salon
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public async Task<IEnumerable<OutPutModel.Output_Salon>> Get()
        {
            try
            {
                var data = (from c in dbContext.TblSalon.AsNoTracking()
                            where c.IsDelete == 0 
                                  && c.Publish == true 
                                  && c.IsSalonHoiQuan == false
                            select new Output_Salon
                            {
                                Id = c.Id
                            }).ToList();

                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
