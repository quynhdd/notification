﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APINotification.Repository.Interface
{
    public interface IBookingRepo
    {
        Task<int?> GetOSById(int Id);
    }
}
