﻿using APINotification.Models.Solution_30Shine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace APINotification.Repository.Interface
{
    public interface INotificationManagementRepo
    {
        void Update(NotificationManagement obj);
        void Insert(NotificationManagement obj);
        void Delete(NotificationManagement obj);
        Task<NotificationManagement> GetById(int id);
        Task<NotificationManagement> Get(Expression<Func<NotificationManagement, bool>> expresstion);
        Task<List<NotificationManagement>> GetList(Expression<Func<NotificationManagement, bool>> expression);
        Task<bool> CheckDuplicateUrl(string url);
        Task<bool> CheckDuplicateName(string name);
        Task SaveChangeAsync();
    }
}
