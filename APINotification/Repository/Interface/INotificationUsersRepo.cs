﻿using APINotification.Models.Solution_30Shine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using static APINotification.Models.CustomeModel.OutPutModel;

namespace APINotification.Repository.Interface
{
    public interface INotificationUsersRepo
    {
        Task<IEnumerable<Output_Notifications>> Get(int userId, int top, string slugkey);
        Task<int> GetTotalNotificationNotSeen(int userId, int top, string slugkey);
        Task<IEnumerable<NotificationUsers>> GetList(Expression<Func<NotificationUsers, bool>> expression);
        Task<NotificationUsers> GetById(Expression<Func<NotificationUsers, bool>> expression);
        void Insert(IEnumerable<NotificationUsers> obj);
        void InsertOnlyRow(NotificationUsers obj);
        void Update(NotificationUsers obj);
        Task SaveChange();
    }
}
