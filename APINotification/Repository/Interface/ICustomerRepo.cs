﻿using APINotification.Models.Solution_30Shine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APINotification.Repository.Interface
{
    public interface ICustomerRepo
    {
        Task<IEnumerable<Customer>> GetByListIds(IEnumerable<int> ids);
    }
}
