﻿using APINotification.Models.Solution_30Shine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using static APINotification.Models.CustomeModel.OutPutModel;

namespace APINotification.Repository.Interface
{
    public interface IStaffRepo
    {
        IEnumerable<Staff> Get(Expression<Func<Staff, bool>> expression);
        IEnumerable<Ouput_Staff> GetList(IEnumerable<int> salonId);
        Task<IEnumerable<Staff>> GetByIds(IEnumerable<int> ids);

    }
}
