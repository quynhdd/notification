﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static APINotification.Models.CustomeModel.OutPutModel;

namespace APINotification.Repository.Interface
{
    public interface IStaffTypeRepo
    {
        Task<IEnumerable<Output_Department>> Get();
    }
}
