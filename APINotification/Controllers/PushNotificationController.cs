﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using APINotification.Extentions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using APINotification.Models.CustomeModel;
using APINotification.Repository.Interface;
using System.Text;
using APINotification.Models.Solution_30Shine;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using static APINotification.Models.CustomeModel.InputModel;

namespace APINotification.Controllers
{
    [Produces("application/json")]
    [Route("api/push-noti")]
    public class PushNotificationController : Controller
    {
        private ILogger<PushNotificationController> Logger { get; }
        private readonly IConfiguration Configuration;
        private readonly IPushNotice PushNotice;
        private readonly IBookingRepo BookingRepo;

        private readonly INotificationManagementRepo NotificationManagementRepo;
        private readonly INotificationUsersRepo NotificationUsersRepo;
        //constructor
        public PushNotificationController(ILogger<PushNotificationController> logger
            , IPushNotice pushNotice, IBookingRepo bookingRepo
            , INotificationManagementRepo notificationManagementRepo
            , IConfiguration configuration
            , INotificationUsersRepo notificationUsersRepo)
        {
            Logger = logger;
            PushNotice = pushNotice;
            BookingRepo = bookingRepo;
            NotificationManagementRepo = notificationManagementRepo;
            Configuration = configuration;
            NotificationUsersRepo = notificationUsersRepo;
        }

        ///// <summary>
        /////  api push thông báo cho app
        ///// </summary>
        ///// <param name="listUser">danh dach id user được thông báo</param>
        ///// <param name="type">loại app 1 = appstaff, 2 = app customer</param>
        ///// <param name="isAllUser">push cho tất cả các app nếu = true</param>
        ///// <returns></returns>
        //[HttpPost]
        //public async Task<IActionResult> PushNoti([FromBody] InputModel.reqPushNotification param)
        //{
        //    try
        //    {
        //        if (param.Type <= 0 || param.ListUser == null) return BadRequest("Data Not exit!");

        //        var appId = "";
        //        var apiKey = "";
        //        switch (param.Type)
        //        {
        //            // = 1 thông báo cho app staff
        //            case 1: appId = AppConstants.ID_APPSTAFF; apiKey = AppConstants.API_KEY_APPSTAFF; break;
        //            // = 2: thông báo cho app customer
        //            case 2: appId = AppConstants.ID_APPCUSTOMER; apiKey = AppConstants.API_KEY_APPCUSTOMER; break;
        //        }
        //        if (param.IsAllUser)
        //        {
        //            // call onesignal bắn all user
        //            //PushNotice.PushStaffCustomer(apiKey, appId, new object[] { }, param.IsAllUser);
        //            return Ok();
        //        }
        //        // Số lượng user được phép bắn thông báo trong 1 lần.
        //        var numberFilteruser = 100;
        //        // số lượng user < 100
        //        if (param.ListUser.Count <= numberFilteruser && param.ListUser.Count > 0)
        //        {
        //            var filters = new object[param.ListUser.Count * 2 - 1];
        //            for (var i = 0; i < param.ListUser.Count; i++)
        //            {
        //                filters[i * 2] = new { field = "tag", key = "user-id", relation = "=", value = param.ListUser[i] };
        //                if (i < param.ListUser.Count - 1)
        //                {
        //                    filters[i * 2 + 1] = new { @operator = "OR" };
        //                }
        //            }
        //            //call xử lý onesignal
        //            //PushNotice.PushStaffCustomer(apiKey, appId, filters, param.IsAllUser);
        //        }
        //        // số lượng user lớn hơn 100
        //        else if (param.ListUser.Count > numberFilteruser)
        //        {
        //            var filters = new object[numberFilteruser * 2 - 1];
        //            var dem = 0;
        //            for (var i = 0; i < param.ListUser.Count; i++)
        //            {
        //                filters[dem * 2] = new { field = "tag", key = "user-id", relation = "=", value = param.ListUser[i] };
        //                if (dem * 2 < filters.Count() - 1)
        //                {
        //                    filters[dem * 2 + 1] = new { @operator = "OR" };
        //                }
        //                dem++;
        //                if (dem == ((filters.Count() + 1) / 2))
        //                {
        //                    dem = 0;
        //                    //PushNotice.PushStaffCustomer(apiKey, appId, filters, param.IsAllUser);
        //                    if (param.ListUser.Count - i < numberFilteruser)
        //                    {
        //                        filters = new object[(param.ListUser.Count - i) * 2 - 2];
        //                    }
        //                    else
        //                    {
        //                        filters = new object[numberFilteruser * 2 - 1];
        //                    }
        //                }
        //            }
        //        }

        //        return Ok("Success!");
        //    }
        //    catch (Exception e)
        //    {
        //        Logger.LogError(e.Message);
        //        PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", Inner: " + e.InnerException);
        //        return StatusCode(500, new { message = e.Message });
        //    }
        //}

        [HttpGet("customer")]
        public async Task<IActionResult> PushForCheckout([FromQuery] int customerId, int bookingId, string phone, string customerName)
        {
            try
            {
                if (customerId <= 0 || bookingId <= 0)
                {
                    return BadRequest("Data Not Exits!");
                }

                var notiId = int.TryParse(Configuration.GetSection("AppSettings:CheckoutNotiId").Value,
                    out var integer) ? integer : 0;

                var data = await BookingRepo.GetOSById(bookingId);
                var objNoti = await NotificationManagementRepo.GetById(notiId);
                if (objNoti == null) return BadRequest("Noti Not Exits!");
                // Chỉ bắn noti khi khách booking qua app, null = tại salon, 1 = tại web. 0 = tại app
                if (data == 0 || data == 2)
                {
                    var notiUser = await NotificationUsersRepo.GetById(r =>
                                                                    r.SlugKey == "customer"
                                                                    && r.UserId == customerId
                                                                    && r.NotiId == notiId);
                    if (notiUser == null)
                    {
                        var obj = new NotificationUsers();
                        obj.NotiId = notiId;
                        obj.UserId = customerId;
                        obj.SlugKey = "customer";
                        obj.Status = 1;
                        obj.IsPublish = true;
                        obj.IsDelete = false;
                        obj.CreatedTime = DateTime.Now;
                        NotificationUsersRepo.InsertOnlyRow(obj);
                        await NotificationUsersRepo.SaveChange();

                        var appId = AppConstants.ID_APPCUSTOMER;
                        var apiKey = AppConstants.API_KEY_APPCUSTOMER;
                        var filters = new object[]
                        {
                            new {field = "tag", key = "user-id", relation = "=", value = customerId}
                        };

                        //PushNotice.PushStaffCustomer(apiKey, appId, filters, objNoti.Title);
                    }
                }
                //Bắn qua brand name khi khách không dùng appp
                //else
                //{
                //    var notiUser = await NotificationUsersRepo.GetById(r =>
                //                                                    r.SlugKey == "brand_name"
                //                                                    && r.UserId == customerId
                //                                                    && r.NotiId == notiId);
                //    if (notiUser == null)
                //    {
                //        var obj = new NotificationUsers();
                //        obj.UserId = customerId;
                //        obj.SlugKey = "brand_name";
                //        obj.Status = 1;
                //        obj.IsPublish = true;
                //        obj.IsDelete = false;
                //        obj.NotiId = notiId;
                //        obj.CreatedTime = DateTime.Now;
                //        NotificationUsersRepo.InsertOnlyRow(obj);
                //        await NotificationUsersRepo.SaveChange();

                //        var message = Configuration.GetSection("AppSettings:MessageNotiCustomer").Value;
                //        message = message.Replace("_TenKhach", customerName);

                //        PushNotice.PushNotiBrandNameVMG(phone, message);
                //    }
                //}
                return Ok();
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", Inner: " + e.InnerException);
                return StatusCode(500, new { message = e.Message });
            }
        }

        /// <summary>
        ///  push noti use brandname vmg
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        //[HttpPost]
        //public IActionResult PushSMSBrandName([FromBody] Input_PushSMS input)
        //{
        //    try
        //    {
        //        var phone = (input.phone + "").Trim();
        //        var message = (input.message + "").Trim();
        //        if (phone == null || phone.Substring(0, 1) != "0" || phone.Length < 10 || phone.Length > 11)
        //        {
        //            return BadRequest(new { message = "Phone number is invalid" });
        //        }
        //        if (message == null || message == "")
        //        {
        //            return BadRequest(new { message = "Message is not exist" });
        //        }
        //        var reponse = PushNotice.PushNotiBrandNameVMG(phone, message);
        //        if (reponse == 403)
        //        {
        //            return BadRequest(new { message = "Không có quyền truy cập dịch vụ gửi SMS VMG" });
        //        }

        //        return Ok(new { message = "success" });
        //    }
        //    catch (Exception e)
        //    {
        //        Logger.LogError(e.Message);
        //        PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", StackTrace: " + e.StackTrace);
        //        return StatusCode(500, new { message = e.Message });
        //    }
        //}

        /// <summary>
        ///  Push noti ratting checkout
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> PushNotiRattingCheckOut([FromBody] Input_PushRatting input)
        {
            try
            {
                if (input.customerId <= 0)
                {
                    PushNotice.PushInfoToSlack("CheckCustomerId: " + input.customerId);
                    return BadRequest(new { message = "Check customerId" });
                }
                if (input.bookingOS < 0)
                {
                    PushNotice.PushInfoToSlack("CheckBookingOS: " + input.bookingOS);
                    return BadRequest(new { message = "Check bookingOS" });
                }
                string checkPushNoti = "";
                // get notiId
                var notiId = int.TryParse(Configuration.GetSection("AppSettings:CheckoutNotiId").Value, out var integer) ? integer : 0;
                // check notiId
                var checkExistNoti = await NotificationManagementRepo.GetById(notiId);
                if (checkExistNoti == null)
                {
                    PushNotice.PushInfoToSlack("CheckNotiExist: " + checkExistNoti);
                    return BadRequest(new { message = "Notification does not exist" });
                }
                // Push noti app customer
                if (input.bookingOS == 0 || input.bookingOS == 1 || input.bookingOS == 2)
                {
                    // get key app customer
                    var appId = AppConstants.ID_APPCUSTOMER;
                    var apiKey = AppConstants.API_KEY_APPCUSTOMER;
                    var filters = new object[]
                    {
                            new {field = "tag", key = "user-id", relation = "=", value = input.customerId}
                    };
                    // call
                    checkPushNoti = PushNotice.PushNotiRatingAppCustomer(apiKey, appId, filters, checkExistNoti.Name, checkExistNoti.Title, checkExistNoti.Images, false, new { type = 1 });
                }
                //
                if ("OK".Equals(checkPushNoti))
                {
                    // 
                    return Ok();
                }
                else
                {
                    PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, input.bookingOS + ", StatusCode: " + checkPushNoti);
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", Inner: " + e.InnerException);
                return StatusCode(500, new { message = e.Message });
            }
        }
    }
}