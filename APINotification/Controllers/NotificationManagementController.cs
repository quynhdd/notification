﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using APINotification.Repository.Interface;
using APINotification.Extentions;
using APINotification.Models.Solution_30Shine;

namespace APINotification.Controllers
{
    [Route("api/notification")]
    /// <summary>
    /// 
    /// </summary>
    public class NotificationManagementController : Controller
    {

        private ILogger<NotificationManagementController> Logger { get; }
        private INotificationManagementRepo NotificationManagementRepo { get; }
        private readonly IPushNotice PushNotice;
        //constructor
        public NotificationManagementController(ILogger<NotificationManagementController> logger, 
                                                INotificationManagementRepo notificationManagementRepo, 
                                                IPushNotice pushNotice)
        {
            Logger = logger;
            NotificationManagementRepo = notificationManagementRepo;
            PushNotice = pushNotice;
        }

        /// <summary>
        /// Get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetById([FromQuery]int id)
        {
            try
            {
                if (id == 0) return BadRequest("Data not exit!");
                var data = await NotificationManagementRepo.GetById(id);
                if (data == null)
                {
                    return NoContent();
                }
                else
                {
                    return Ok(data);
                }

            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", Inner: " + e.InnerException);
                return StatusCode(500, new { message = e.Message });
            }
        }

        /// <summary>
        /// Insert NotificationManagement
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Insert([FromBody] NotificationManagement param)
        {
            try
            {
                if (param == null) return BadRequest("Data not exit!");
                NotificationManagementRepo.Insert(param);
                await NotificationManagementRepo.SaveChangeAsync();
                return Ok("Insert Success!");

            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", Inner: " + e.InnerException);
                return StatusCode(500, new { message = e.Message });
            }
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> Update([FromBody] NotificationManagement param)
        {
            try
            {

                if (param == null || param.Id == 0) return BadRequest("Data not exit!");
                NotificationManagementRepo.Update(param);
                await NotificationManagementRepo.SaveChangeAsync();
                return Ok("Update Success!");
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", Inner: " + e.InnerException);
                return StatusCode(500, new { message = e.Message });
            }
        }

        /// <summary>
        /// Delete notification
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> Delete([FromQuery] int id)
        {
            try
            {
                if (id == 0) return BadRequest("Data not exit!");
                var record = await NotificationManagementRepo.GetById(id);
                if (record != null)
                {
                    NotificationManagementRepo.Delete(record);
                    await NotificationManagementRepo.SaveChangeAsync();
                    return Ok( new { message = "Delete Success!" });
                }
                else
                {
                    return BadRequest("Data not exit!");
                }
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", Inner: " + e.InnerException);
                return StatusCode(500, new { message = e.Message });
            }
        }

        /// <summary>
        /// check duplicate url
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        [HttpGet("check-duplicate-url")]
        public async Task<IActionResult> CheckDuplicateUrl([FromQuery]string url)
        {
            try
            {
                if (string.IsNullOrEmpty(url)) return BadRequest("Data Not exit!"); 
                var data = await NotificationManagementRepo.CheckDuplicateUrl(url);
                return Ok(data);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", Inner: " + e.InnerException);
                return StatusCode(500, new { message = e.Message });
            }
        }

        /// <summary>
        /// Check duplicate name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet("check-duplicate-name")]
        public async Task<IActionResult> CheckDuplicateName([FromQuery]string name)
        {
            try
            {
                if (string.IsNullOrEmpty(name)) return BadRequest("Data Not exit!");
                var data = await NotificationManagementRepo.CheckDuplicateName(name);
                return Ok(data);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", Inner: " + e.InnerException);
                return StatusCode(500, new { message = e.Message });
            }
        }

        /// <summary>
        /// Get list notification
        /// </summary>
        /// <param name="type"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet("list-noti")]
        public async Task<IActionResult> GetList([FromQuery] string name)
        {
            try
            {
                name = name ?? "";
                var data = await NotificationManagementRepo.GetList(r=>((r.Name == name && name != "") || name == "") && r.IsDelete == false);
                return Ok(data);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", Inner: " + e.InnerException);
                return StatusCode(500, new { message = e.Message });
            }
        }

        /// <summary>
        /// Get name noti
        /// </summary>
        /// <returns></returns>
        [HttpGet("get-name")]
        public async Task<IActionResult> GetByName()
        {
            try
            {
                var data = await NotificationManagementRepo.GetList(r => r.IsDelete == false);
                if (data != null)
                {
                    return Ok(data.Select(r => r.Name));
                }
                else
                {
                    return NoContent();
                }
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", Inner: " + e.InnerException);
                return StatusCode(500, new { message = e.Message });
            }
        }
    }
}