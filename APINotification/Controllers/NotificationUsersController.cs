﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APINotification.Extentions;
using APINotification.Models.Solution_30Shine;
using APINotification.Repository.Interface;
using APINotification.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using static APINotification.Models.CustomeModel.InputModel;

namespace APINotification.Controllers
{
    [Produces("application/json")]
    [Route("api/notification-users")]
    public class NotificationUsersController : Controller
    {
        private ILogger<NotificationUsersController> Logger { get; }
        private readonly IPushNotice PushNotice;
        private readonly INotificationUsersRepo NotificationUsersRepo;
        private readonly IStaffRepo StaffRepo;
        private readonly ISalonRepo SalonRepo;
        private readonly IStaffTypeRepo StaffTypeRepo;
        private readonly INotificationManagementRepo NotificationManagementRepo;

        //constructor
        public NotificationUsersController(ILogger<NotificationUsersController> logger,
            IPushNotice pushNotice,
            INotificationUsersRepo notificationUsersRepo,
            IStaffRepo staffRepo,
            ISalonRepo salonRepo,
            IStaffTypeRepo staffTypeRepo,
            INotificationManagementRepo notificationManagementRepo)
        {
            Logger = logger;
            PushNotice = pushNotice;
            NotificationUsersRepo = notificationUsersRepo;
            StaffRepo = staffRepo;
            SalonRepo = salonRepo;
            StaffTypeRepo = staffTypeRepo;
            NotificationManagementRepo = notificationManagementRepo;
        }

        /// <summary>
        /// Insert danh sách user nhận noti theo các điều kiện chọn
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///         {
        ///           "salonId": "2,3,4",
        ///           "departmentId": "1,2",
        ///           "notiId": 1
        ///         }
        ///
        /// </remarks>
        [HttpPost]
        public async Task<IActionResult> Add([FromBody] Input_InsertUser data)
        {
            try
            {
                if (data == null)
                {
                    return BadRequest(new { message = "Data does not exist!" });
                }
                if (string.IsNullOrEmpty(data.SalonId))
                {
                    return BadRequest(new { message = "SalonId not exist!" });
                }
                if (string.IsNullOrEmpty(data.DepartmentId))
                {
                    return BadRequest(new { message = "DepartmentId not exist!" });
                }
                if (data.NotiId <= 0)
                {
                    return BadRequest(new { message = "Check input NotiId!" });
                }
                //Convert chuỗi SalonId
                var listSalonId = data.SalonId.ConvertString();
                // get list staff by SalonId
                var listStaffBySalonId = StaffRepo.GetList(listSalonId);
                // Convert chuỗi DepartmentId
                var listDepartmentId = data.DepartmentId.ConvertString();
                // get list staff by departmentId
                var listStaffByDepartmentId = listStaffBySalonId.Where(w => listDepartmentId.Contains(w.DepartmentId)).Select(a => a.Id);
                // Get list notificationuser
                var listNoti = await NotificationUsersRepo.GetList(r => r.NotiId == data.NotiId && r.IsDelete == false);
                // get title Noti
                var getdataNotimanagement = await NotificationManagementRepo.Get(r => r.Id == data.NotiId);
                if (getdataNotimanagement == null)
                {
                    return NoContent();
                }
                var messageName = getdataNotimanagement.Name;
                var messageTitle = getdataNotimanagement.Title;
                var messagePicture = getdataNotimanagement.Images;
                var list = new List<NotificationUsers>();
                foreach (var v in listStaffByDepartmentId)
                {
                    // check Duplicate UserId
                    var checkDuplicate = listNoti.FirstOrDefault(r => r.UserId == v);
                    if (checkDuplicate == null)
                    {
                        var obj = new NotificationUsers();
                        obj.NotiId = data.NotiId;
                        obj.UserId = v;
                        obj.SlugKey = "staff";
                        obj.Status = 1;
                        obj.IsPublish = true;
                        obj.IsDelete = false;
                        obj.CreatedTime = DateTime.Now;
                        list.Add(obj);
                    }
                }
                //insert
                NotificationUsersRepo.Insert(list);
                //save change
                await NotificationUsersRepo.SaveChange();
                //get list Salon 
                //var ListSalonId = await SalonRepo.Get();
                //get list departmentId
                //var ListDepartmentId = await StaffTypeRepo.Get();
                //check nếu list salon và list department truyền vào là All thì mặc định bắn toàn bộ noti cho appstaff
                //if (ListSalonId.Count() > 0 && ListSalonId.Count() == listSalonId.Count() && ListDepartmentId.Count() > 0 && ListDepartmentId.Count() == listDepartmentId.Count())
                //{
                //PushNotice.PushNotificationOnesignal(new List<int>(), 1, true, messageName, messageTitle, messagePicture);
                //}
                //chọn 1 or nhiều Salon, 1 or nhiều bộ phận thì sẽ bắn theo từng User
                //else
                //{
                if (list.Count > 0)
                {
                    //select list staffId
                    var listUserId = list.Select(a => a.UserId ?? 0).ToList();
                    //call push notification use onesignal
                    PushNotice.PushNotificationOnesignal(listUserId, 1, false, messageName, messageTitle, messagePicture, new { type= 0, link = getdataNotimanagement.Url});
                }
                //}

                return Ok("Success!");
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", Inner: " + e.InnerException);
                return StatusCode(500, new { message = e.Message });
            }
        }

        /// <summary>
        /// Insert danh sách user nhận noti từ file excel
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///       {
        ///         "notiId": 1,
        ///         "data": [
        ///           {
        ///             "userId": 16,
        ///             "slugkey": "staff"
        ///           },
        ///           {
        ///             "userId": 51,
        ///             "slugkey": "staff"
        ///           }
        ///         ]
        ///      }
        ///
        /// </remarks>
        [HttpPost("import-file-excel")]
        public async Task<IActionResult> Insert([FromBody] Input_ImportExcel input)
        {
            try
            {
                if (input == null)
                {
                    return BadRequest(new { message = "Data does not exist!" });
                }
                if (input.NotiId <= 0)
                {
                    return BadRequest(new { message = "Check input NotiId!" });
                }
                if (input.data.Count <= 0)
                {
                    return BadRequest(new { message = "Data file excel not exist!" });
                }
                var userId = input.data.Select(r => r.UserId).Distinct();
                foreach (var v in userId)
                {
                    var checkData = input.data.Where(a => a.UserId == v).Select(a => new
                    {
                        slugkey = a.Slugkey
                    });
                    if (checkData.Distinct().Count() != checkData.Count())
                    {
                        return BadRequest("Duplicate userId file excel: UserId = " + v + "!");
                    }
                }
                // Get list notificationuser
                var listNoti = await NotificationUsersRepo.GetList(r => r.NotiId == input.NotiId && r.IsDelete == false);
                // get title Noti
                var getdataNotimanagement = await NotificationManagementRepo.Get(r => r.Id == input.NotiId);
                if (getdataNotimanagement == null)
                {
                    return NoContent();
                }
                var messageName = getdataNotimanagement.Name;
                var messageTitle = getdataNotimanagement.Title;
                var messagePicture = getdataNotimanagement.Images;
                var list = new List<NotificationUsers>();
                foreach (var v in input.data)
                {
                    // check Duplicate UserId
                    var checkDuplicate = listNoti.FirstOrDefault(r => r.UserId == v.UserId && r.SlugKey == v.Slugkey);
                    if (checkDuplicate == null)
                    {
                        var obj = new NotificationUsers();
                        obj.NotiId = input.NotiId;
                        obj.UserId = v.UserId;
                        obj.SlugKey = v.Slugkey;
                        obj.Status = 1;
                        obj.IsPublish = true;
                        obj.IsDelete = false;
                        obj.CreatedTime = DateTime.Now;
                        list.Add(obj);
                    }
                }
                //insert
                NotificationUsersRepo.Insert(list);
                //save change
                await NotificationUsersRepo.SaveChange();
                //check list
                if (list.Count > 0)
                {
                    //select list staffId where slugkey = staff
                    var listStaffId = list.Where(w => w.SlugKey == "staff").Select(a => a.UserId ?? 0).ToList();
                    if (listStaffId.Count() > 0)
                    {
                        //call push notification staff Id use onesignal
                        PushNotice.PushNotificationOnesignal(listStaffId, 1, false, messageName, messageTitle, messagePicture, new { type=0, link = getdataNotimanagement.Url});
                    }
                    // select list customerid where slugley = customer
                    var listCustomerId = list.Where(w => w.SlugKey == "customer").Select(a => a.UserId ?? 0).ToList();
                    if (listCustomerId.Count() > 0)
                    {
                        //call push notification customer Id use onesignal
                        PushNotice.PushNotificationOnesignal(listCustomerId, 2, false, messageName, messageTitle, messagePicture ,  new { type=0 , link = getdataNotimanagement.Url});
                    }
                }

                return Ok("Success!");
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", Inner: " + e.InnerException);
                return StatusCode(500, new { message = e.Message });
            }
        }

        /// <summary>
        /// Get list notification by app
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="notiId"></param>
        /// <param name="top"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetListNotification([FromQuery] int userId, [FromQuery]int top, [FromQuery] string slugkey)
        {
            try
            {
                if (userId <= 0)
                {
                    return BadRequest("Data does not exist!");
                }
                var data = await NotificationUsersRepo.Get(userId, top, slugkey);

                return Ok(data);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", Inner: " + e.InnerException);
                return StatusCode(500, new { message = e.Message });
            }
        }

        /// <summary>
        /// Get list notification chưa đọc 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="notiId"></param>
        /// <param name="top"></param>
        /// <returns></returns>
        [HttpGet("get-total-notification")]
        public async Task<IActionResult> GetTotalNotificationNotSeen([FromQuery] int userId, [FromQuery]int top, [FromQuery] string slugkey)
        {
            try
            {
                if (userId <= 0)
                {
                    return BadRequest("Data does not exist!");
                }
                var data = await NotificationUsersRepo.GetTotalNotificationNotSeen(userId, top, slugkey);

                return Ok(new { total = data });
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", Inner: " + e.InnerException);
                return StatusCode(500, new { message = e.Message });
            }
        }

        /// <summary>
        /// Update status notifications
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///         "userId":51,
        ///         "notiId":1,
        ///         "status":1
        ///     }
        ///
        /// </remarks>
        [HttpPut]
        public async Task<IActionResult> Update([FromBody] Input_Noti input)
        {
            try
            {
                if (input.UserId <= 0 || input.NotiId <= 0)
                {
                    return BadRequest("Data does not exist!");
                }
                var data = await NotificationUsersRepo.GetById(r => r.UserId == input.UserId
                                                                && r.NotiId == input.NotiId
                                                                && r.IsDelete == false
                                                                && (r.SlugKey == "customer" || r.SlugKey == "staff"));
                if (data == null)
                {
                    return NoContent();
                }
                data.Status = input.Status;
                data.ModifiedTime = DateTime.Now;
                NotificationUsersRepo.Update(data);
                await NotificationUsersRepo.SaveChange();

                return Ok("Success!");
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", Inner: " + e.InnerException);
                return StatusCode(500, new { message = e.Message });
            }
        }
    }
}