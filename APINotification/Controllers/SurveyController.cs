﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using APINotification.Extentions;
using APINotification.Models.Dynamo;
using APINotification.Models.SurveyModel;
using APINotification.Repository.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using static APINotification.Models.SurveyModel.QuestionJson;

namespace APINotification.Controllers
{
    [Produces("application/json")]
    [Route("api/survey")]
    public class SurveyController : Controller
    {
        private IDynamoDbContext<SurveyQuestion> SurveyQuestionContext;
        private IDynamoDbContext<SurveyAnswer> SurveyAnswerContext;
        private IDynamoDbContext<SurveyUser> SurveyUserContext;
        private IDynamoDbContext<SurveyQuestionType> SurveyQuestionTypeContext;
        private ICustomerRepo CustomerRepo;
        private IStaffRepo StaffRepo;
        private readonly IPushNotice PushNotice;

        public SurveyController(IDynamoDbContext<SurveyQuestion> surveyQuestionContext, IDynamoDbContext<SurveyAnswer> surveyAnswerContext, IDynamoDbContext<SurveyUser> surveyUserContext, IDynamoDbContext<SurveyQuestionType> surveyQuestionTypeContext, ICustomerRepo customerRepo, IStaffRepo staffRepo, IPushNotice pushNotice)
        {
            SurveyQuestionContext = surveyQuestionContext;
            SurveyAnswerContext = surveyAnswerContext;
            SurveyUserContext = surveyUserContext;
            SurveyQuestionTypeContext = surveyQuestionTypeContext;
            CustomerRepo = customerRepo;
            StaffRepo = staffRepo;
            PushNotice = pushNotice;
        }

        [HttpPost]
        public async Task<IActionResult> PostQuestion([FromBody] PostJson data)
        {
            try
            {
                if (data == null)
                {
                    return BadRequest(new { Message = "Data can not be null" });
                }

                if (string.IsNullOrEmpty(data.Question) || data.QuestionType <= 0 || string.IsNullOrEmpty(data.StartDate) || string.IsNullOrEmpty(data.EndDate))
                {
                    return BadRequest(new { Message = "Invalid Data" });
                }

                var startDate = DateTime.ParseExact(data.StartDate, @"dd-MM-yyyy", CultureInfo.InvariantCulture);
                var endDate = DateTime.ParseExact(data.EndDate, @"dd-MM-yyyy", CultureInfo.InvariantCulture);

                if (startDate == null || endDate == null)
                {
                    return BadRequest(new { Message = "Can not convert date" });
                }

                var question = new SurveyQuestion
                {
                    Id = Guid.NewGuid().ToString(),
                    Type = data.QuestionType,
                    Question = data.Question,
                    StartDate = startDate,
                    EndDate = endDate,
                    Image = data.Image,
                    CreatedDate = DateTime.Now,
                    IsDelete = false,
                    UserType = data.UserType,
                    NotiTitle = data.NotiTitle,
                    NotiContent = data.NotiContent
                };

                if (data.QuestionType == AppConstants.SURVEY_QUESTION_MULTIPLE_CHOICE && data.Answers.Count() != 4)
                {
                    return BadRequest(new { Message = "Number of answer must be 4" });
                }

                await SurveyQuestionContext.SaveAsync(question);

                if (data.QuestionType == AppConstants.SURVEY_QUESTION_MULTIPLE_CHOICE)
                {
                    foreach (var item in data.Answers)
                    {
                        var answer = new SurveyAnswer
                        {
                            Id = Guid.NewGuid().ToString(),
                            QuestionId = question.Id,
                            Answer = item.Answer,
                            CreatedDate = DateTime.Now,
                            IsDelete = false
                        };

                        await SurveyAnswerContext.SaveAsync(answer);
                    }
                }

                return Ok(question);

            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutQuestion(string id, [FromBody] PostJson data)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return BadRequest(new { Message = "Invalid Id" });
                }

                var question = await SurveyQuestionContext.GetByIdAsync(id);

                if (question == null)
                {
                    return NoContent();
                }

                if (data == null)
                {
                    return BadRequest(new { Message = "Data can not be null" });
                }

                if (string.IsNullOrEmpty(data.Question) || data.QuestionType <= 0 || string.IsNullOrEmpty(data.StartDate) || string.IsNullOrEmpty(data.EndDate))
                {
                    return BadRequest(new { Message = "Invalid Data" });
                }

                var startDate = DateTime.ParseExact(data.StartDate, @"dd-MM-yyyy", CultureInfo.InvariantCulture);
                var endDate = DateTime.ParseExact(data.EndDate, @"dd-MM-yyyy", CultureInfo.InvariantCulture);

                if (startDate == null || endDate == null)
                {
                    return BadRequest(new { Message = "Can not convert date" });
                }

                if (data.QuestionType == AppConstants.SURVEY_QUESTION_MULTIPLE_CHOICE && data.Answers.Count() != 4)
                {
                    return BadRequest(new { Message = "Number of answer must be 4" });
                }

                question.Question = data.Question;
                question.StartDate = startDate;
                question.EndDate = endDate;
                question.Type = data.QuestionType;
                question.Image = data.Image;
                question.ModifiedDate = DateTime.Now;
                question.UserType = data.UserType;
                question.NotiTitle = data.NotiTitle;
                question.NotiContent = data.NotiContent;

                await SurveyQuestionContext.SaveAsync(question);

                if (data.QuestionType == AppConstants.SURVEY_QUESTION_MULTIPLE_CHOICE)
                {
                    List<ScanCondition> scanConditions = new List<ScanCondition>();
                    scanConditions.Add(new ScanCondition("QuestionId", ScanOperator.Equal, id));

                    var answers = await SurveyAnswerContext.GetByAttribute(scanConditions);

                    if (answers.Any())
                    {
                        foreach (var item in answers)
                        {
                            await SurveyAnswerContext.DeleteByIdAsync(item);
                        }
                    }

                    foreach (var item in data.Answers)
                    {
                        var answer = new SurveyAnswer
                        {
                            Id = Guid.NewGuid().ToString(),
                            QuestionId = question.Id,
                            Answer = item.Answer,
                            CreatedDate = DateTime.Now,
                            IsDelete = false
                        };

                        await SurveyAnswerContext.SaveAsync(answer);
                    }

                }

                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpGet("question-type")]
        public async Task<IActionResult> GetQuestionType()
        {
            try
            {
                var questionType = await SurveyQuestionTypeContext.GetAll();

                if (!questionType.Any())
                {
                    return NoContent();
                }

                return Ok(questionType);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpGet("list-question")]
        public async Task<IActionResult> GetListSurvey([FromQuery] string startDate, string endDate)
        {
            try
            {
                if (string.IsNullOrEmpty(startDate) || string.IsNullOrEmpty(endDate))
                {
                    return BadRequest(new { Message = "Invalid date" });
                }

                var start = DateTime.ParseExact(startDate, @"dd-MM-yyyy", CultureInfo.InvariantCulture);
                var end = DateTime.ParseExact(endDate, @"dd-MM-yyyy", CultureInfo.InvariantCulture);

                if (start == null || end == null)
                {
                    return BadRequest(new { Message = "Can not convert date" });
                }

                List<ScanCondition> scanConditions = new List<ScanCondition>();
                scanConditions.Add(new ScanCondition("StartDate", ScanOperator.Between, start, end));
                //scanConditions.Add(new ScanCondition("EndDate", ScanOperator.Between, start, end));

                var questions = await SurveyQuestionContext.GetByAttribute(scanConditions);

                if (!questions.Any())
                {
                    return NoContent();
                }

                var questionType = await SurveyQuestionTypeContext.GetAll();

                return Ok(questions.Select(a => new
                {
                    id = a.Id,
                    type = a.Type,
                    typeName = questionType.Where(b => b.QuestionType == a.Type).FirstOrDefault().QuestionTypeName,
                    question = a.Question,
                    startDate = a.StartDate.ToString("dd-MM-yyyy"),
                    endDate = a.EndDate.ToString("dd-MM-yyyy"),
                    image = a.Image,
                    userType = a.UserType,
                    notiTitle = a.NotiTitle,
                    notiContent = a.NotiContent,
                    a.CreatedDate
                }).OrderByDescending(a => a.CreatedDate));

            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetQuestionById(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return BadRequest(new { Message = "Invalid Id" });
            }

            var question = await SurveyQuestionContext.GetByIdAsync(id);

            if (question == null)
            {
                return NoContent();
            }

            var questionType = await SurveyQuestionTypeContext.GetAll();

            if (question.Type == AppConstants.SURVEY_QUESTION_MULTIPLE_CHOICE)
            {
                List<ScanCondition> scanConditions = new List<ScanCondition>();
                scanConditions.Add(new ScanCondition("QuestionId", ScanOperator.Equal, id));

                var answers = await SurveyAnswerContext.GetByAttribute(scanConditions);

                if (answers.Any())
                {
                    var data = new
                    {
                        question.Id,
                        question.Question,
                        question.Type,
                        typeName = questionType.Where(b => b.QuestionType == question.Type).FirstOrDefault().QuestionTypeName,
                        startDate = question.StartDate.ToString("dd-MM-yyyy"),
                        endDate = question.EndDate.ToString("dd-MM-yyyy"),
                        question.UserType,
                        notiTitle = question.NotiTitle,
                        notiContent = question.NotiContent,
                        image = question.Image,
                        answers = answers.Select(a => new { a.Answer })
                    };

                    return Ok(data);
                }
            }

            var returnData = new
            {
                question.Id,
                question.Question,
                question.Type,
                typeName = questionType.Where(b => b.QuestionType == question.Type).FirstOrDefault().QuestionTypeName,
                startDate = question.StartDate.ToString("dd-MM-yyyy"),
                endDate = question.EndDate.ToString("dd-MM-yyyy"),
                question.UserType,
                notiTitle = question.NotiTitle,
                notiContent = question.NotiContent,
                image = question.Image,
                answers = new object()
            };

            return Ok(returnData);
        }

        [HttpPost("user")]
        public async Task<IActionResult> PostUser([FromBody] UserJson data)
        {
            try
            {
                if (data == null)
                {
                    return BadRequest(new { Message = "Data can not be null" });
                }

                if (string.IsNullOrEmpty(data.UserIds))
                {
                    return BadRequest(new { Message = "UserIds can not be null" });
                }

                var question = await SurveyQuestionContext.GetByIdAsync(data.QuestionId);

                if (question == null)
                {
                    return BadRequest(new { Message = "Question not found" });
                }

                var userIds = data.UserIds.Split(',').Select(Int32.Parse).Distinct().ToList();

                if (userIds.Count() > 1000)
                {
                    return BadRequest(new { Message = "Reach User Limited (1000)" });
                }

                var ids = new List<int>();

                if (question.UserType == 2)
                {
                    var customer = await CustomerRepo.GetByListIds(userIds);

                    ids = customer.Select(a => a.Id).ToList();
                }
                else
                {
                    var staff = await StaffRepo.GetByIds(userIds);

                    ids = staff.Select(a => a.Id).ToList();
                }

                var invalidUser = userIds.Where(a => !ids.Contains(a)).ToList();

                userIds.RemoveAll(a => invalidUser.Contains(a));

                var exsit = new List<int>();

                var listUserId = new List<int>();

                foreach (var item in userIds)
                {

                    List<ScanCondition> scanConditions = new List<ScanCondition>();
                    scanConditions.Add(new ScanCondition("UserId", ScanOperator.Equal, item));
                    scanConditions.Add(new ScanCondition("UserType", ScanOperator.Equal, question.UserType));

                    var surveyUsers = await SurveyUserContext.GetByAttribute(scanConditions);

                    if (surveyUsers.Any())
                    {
                        var questionId = surveyUsers.FirstOrDefault().QuestionId;

                        //List<ScanCondition> scanCondition = new List<ScanCondition>();
                        //scanCondition.Add(new ScanCondition("Id", ScanOperator.In, questionIds.ToArray()));

                        var userQuestion = await SurveyQuestionContext.GetByIdAsync(questionId);

                        if (userQuestion.EndDate >= DateTime.Today)
                        {
                            exsit.Add(item);
                        }
                        else
                        {
                            await SurveyUserContext.DeleteByIdAsync(surveyUsers.FirstOrDefault());

                            var user = new SurveyUser
                            {
                                UserId = item,
                                UserType = question.UserType,
                                IsAnswer = false,
                                QuestionId = data.QuestionId,
                                IsDelete = false,
                                CreatedDate = DateTime.Now
                            };

                            await SurveyUserContext.SaveAsync(user);

                            listUserId.Add(item);
                        }
                    }
                    else
                    {
                        var user = new SurveyUser
                        {
                            UserId = item,
                            UserType = question.UserType,
                            IsAnswer = false,
                            QuestionId = data.QuestionId,
                            IsDelete = false,
                            CreatedDate = DateTime.Now
                        };

                        await SurveyUserContext.SaveAsync(user);

                        listUserId.Add(item);
                    }

                }

                PushNotice.PushNotificationOnesignal(listUserId, question.UserType, false, question.NotiTitle, question.NotiContent, string.Empty, new { type = 2 });

                var invalid = new List<InvalidJson>();

                invalid.AddRange(exsit.Select(a => new InvalidJson
                {
                    Type = "Đã Tồn Tại",
                    UserId = a
                }));

                invalid.AddRange(invalidUser.Select(a => new InvalidJson
                {
                    Type = "Khách Hàng Không Tồn Tại",
                    UserId = a
                }));

                invalid.AddRange(data.UserIds.Split(',').Select(Int32.Parse).GroupBy(x => x)
                        .Where(a => a.Count() > 1)
                        .Select(a => new InvalidJson { Type = "Duplicate", UserId = a.Key}));

                return Ok(invalid);
                
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteOutOfDateQuestion()
        {
            try
            {
                var questions = await SurveyQuestionContext.GetAll();

                questions = questions.Where(a => a.EndDate < DateTime.Today).ToList();

                if (questions.Any())
                {

                    var questionIds = questions.Select(a => a.Id).ToList();

                    foreach (var question in questions)
                    {
                        await SurveyQuestionContext.DeleteByIdAsync(question);

                        if (question.Type == 3)
                        {
                            var scanConditions = new List<ScanCondition>();
                            scanConditions.Add(new ScanCondition("QuestionId", ScanOperator.Equal, question.Id));

                            var answers = await SurveyAnswerContext.GetByAttribute(scanConditions);

                            if (answers.Any())
                            {
                                foreach (var item in answers)
                                {
                                    await SurveyAnswerContext.DeleteByIdAsync(item);
                                }
                            }
                        }
                    }


                    var conditions = new List<ScanCondition>();
                    conditions.Add(new ScanCondition("QuestionId", ScanOperator.In, questionIds.ToArray()));

                    var user = await SurveyUserContext.GetByAttribute(conditions);

                    if (user.Any())
                    {
                        foreach (var item in user)
                        {
                            await SurveyUserContext.DeleteByIdAsync(item);
                        }
                    }
                }

                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteQuestion(string id)
        {
            try
            {
                var question = await SurveyQuestionContext.GetByIdAsync(id);

                if (question == null)
                {
                    return NoContent();
                }

                await SurveyQuestionContext.DeleteByIdAsync(question);

                if (question.Type == 3)
                {
                    var scanConditions = new List<ScanCondition>();
                    scanConditions.Add(new ScanCondition("QuestionId", ScanOperator.Equal, question.Id));

                    var answers = await SurveyAnswerContext.GetByAttribute(scanConditions);

                    if (answers.Any())
                    {
                        foreach (var item in answers)
                        {
                            await SurveyAnswerContext.DeleteByIdAsync(item);
                        }
                    }
                }

                var conditions = new List<ScanCondition>();
                conditions.Add(new ScanCondition("QuestionId", ScanOperator.Equal, question.Id));

                var user = await SurveyUserContext.GetByAttribute(conditions);

                if (user.Any())
                {
                    foreach (var item in user)
                    {
                        await SurveyUserContext.DeleteByIdAsync(item);
                    }
                }

                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        public class InvalidJson
        {
            public string Type { get; set; }
            public int UserId { get; set; }
        }
    }
}