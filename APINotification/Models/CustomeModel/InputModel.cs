﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APINotification.Models.CustomeModel
{
    public class InputModel
    {
        #region Query params
        public class QueryStylist
        {
            public int SalonId { get; set; }
            public string WorkDate { get; set; }
        }

        public class QueryBookHour
        {
            public int SalonId { get; set; }
            public int StylistId { get; set; }
            public string DateBook { get; set; }
            public string CustomerPhone { get; set; }
        }

        public class QueryMenbershipRanking
        {
            public string Phone { get; set; }
            public string From { get; set; }
            public string To { get; set; }
        }

        public class QueryBooking
        {
            public string Phone { get; set; }
            public string BookDate { get; set; }
        }
        public class QueryHourBooking
        {
            public int hourId { get; set; }
            public TimeSpan? hourFram { get; set; }
        }

        #endregion

        #region Request header
        public class ReqHeadCustomerLogin
        {
            public string CustomerPhone { get; set; }
            public string DeviceId { get; set; }
            public string Ip { get; set; }
            public string AppId { get; set; }
            public string AppVersion { get; set; }
        }

        #endregion

        #region Request data


        public class ReqDataCustomer
        {
            public string Phone { get; set; }
            public string Name { get; set; }
            public string Dob { get; set; }
            public string Email { get; set; }
            public int SalonId { get; set; }
            public string Address { get; set; }
            public string Avatar { get; set; }
            public string FacebookId { get; set; }
            public string FacebookName { get; set; }
        }

        public class ReqDataOTP
        {
            public string Phone { get; set; }
            public string Code { get; set; }
        }

        public class ReqDataBooking
        {
            public string Phone { get; set; }
            public int SalonId { get; set; }
            public int StylistId { get; set; }
            public string DateBook { get; set; }
            public int HourBookId { get; set; }
            public string NoteBeforBook { get; set; }
            public string NoteAfterBook { get; set; }

            //(timeline: 0; web: 1; app customer: 2)
            public string BookAt { get; set; }
        }
        public class reqPushNotification
        {
            public List<int> ListUser { get; set; }
            public int Type { get; set; }
            public bool IsAllUser { get; set; }
        }
        #endregion

        #region Response data
        public class ResDataUser
        {
            public int Id { get; set; }
            public string Account { get; set; }
            public string Password { get; set; }
            public string Name { get; set; }
            public string Email { get; set; }
            public string Phone { get; set; }
        }


        #endregion

        #region[Authen]
        public class AuthenModel
        {
            public string Phone { get; set; }
            public string DeviceId { get; set; }
            public string Ip { get; set; }
            public string AppId { get; set; }
            public string AppVersion { get; set; }
            public ResDataUser User { get; set; }
            public object Perm { get; set; }
        }
        #endregion

        public class Input_Noti
        {
            public int UserId { get; set; }
            public int NotiId { get; set; }
            public int Status { get; set; }
        }

        /// <summary>
        /// Input_InsertUser
        /// </summary>
        public class Input_InsertUser
        {
            public string SalonId { get; set; }
            public string DepartmentId { get; set; }
            public int NotiId { get; set; }
        }

        /// <summary>
        /// Input_ImportExcel
        /// </summary>
        public class Input_ImportExcel
        {
            public int NotiId { get; set; }
            public List<ImportExcel> data { get; set; }
        }

        /// <summary>
        /// ImportExcel
        /// </summary>
        public class ImportExcel
        {
            public int UserId { get; set; }
            public string Slugkey { get; set; }
        }


        public class Input_PushSMS
        {
            public string phone { get; set; }
            public string message { get; set; }
        }

        public class Input_PushRatting
        {
            public int customerId { get; set; }
            public int bookingOS { get; set; }
        }
    }
}
