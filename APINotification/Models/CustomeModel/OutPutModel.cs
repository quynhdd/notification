﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APINotification.Models.CustomeModel
{
    public class OutPutModel
    {
        public class Output_Notifications
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Title { get; set; }
            public string Descriptions { get; set; }
            public string Url { get; set; }
            public string Image { get; set; }
            public string CreatedTime { get; set; }
            public int Status { get; set; }
            public int Type { get; set; }
        }

        public class Output_TotalSendNotifications
        {
            public int Total { get; set; }
        }

        public class Ouput_Staff
        {
            public int Id { get; set; }
            public int DepartmentId { get; set; }
        }
        public class MessageInfor
        {
            public Byte Status { get; set; }
            public string Message { get; set; }
        }

        /// <summary>
        /// Output list Salon
        /// </summary>
        public class Output_Salon
        {
            public int Id { get; set; }
        }

        /// <summary>
        /// Output list Department
        /// </summary>
        public class Output_Department
        {
            public int Id { get; set; }
        }
    }
}
