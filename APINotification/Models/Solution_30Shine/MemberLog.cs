﻿using System;
using System.Collections.Generic;

namespace APINotification.Models.Solution_30Shine
{
    public partial class MemberLog
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string Phone { get; set; }
        public int ProductId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int Amount { get; set; }
        public int? IsDelete { get; set; }
        public int BillId { get; set; }
    }
}
