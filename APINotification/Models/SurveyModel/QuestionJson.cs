﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APINotification.Models.SurveyModel
{
    public class QuestionJson
    {
        public class PostJson
        {
            public string Question { get; set; }
            public int QuestionType { get; set; }
            public string StartDate { get; set; }
            public string EndDate { get; set; }
            public string Image { get; set; }
            public int UserType { get; set; }
            public string NotiTitle { get; set; }
            public string NotiContent { get; set; }
            public List<Answers> Answers { get; set; }
        }

        public class Answers
        {
            public string Answer { get; set; }
        }
    }
}
