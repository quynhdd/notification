﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APINotification.Models.SurveyModel
{
    public class UserJson
    {
        public string QuestionId { get; set; }
        public string UserIds { get; set; }
    }
}
