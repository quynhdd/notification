﻿using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APINotification.Models.Dynamo
{
    public class DynamoDbContext<T> : DynamoDBContext, IDynamoDbContext<T> where T : class
    {
        private DynamoDBOperationConfig _config;

        public DynamoDbContext(IAmazonDynamoDB client, string tableName) : base(client)
        {
            _config = new DynamoDBOperationConfig()
            {
                OverrideTableName = tableName
            };
        }

        public async Task<T> GetByIdAsync(string id)
        {
            return await base.LoadAsync<T>(id, _config);
        }

        public async Task SaveAsync(T item)
        {
            await base.SaveAsync(item, _config);
        }

        public async Task SaveAsync(List<T> item)
        {
            await base.SaveAsync(item, _config);
        }

        public async Task DeleteByIdAsync(T item)
        {
            await base.DeleteAsync(item, _config);
        }

        public async Task<List<T>> GetAll()
        {
            var conditions = new List<ScanCondition>();

            return await base.ScanAsync<T>(conditions).GetRemainingAsync();

        }

        public async Task<List<T>> GetByAttribute(List<ScanCondition> scanConditions)
        {
            return await base.ScanAsync<T>(scanConditions).GetRemainingAsync();
        }
    }
}
