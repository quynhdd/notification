﻿using Amazon.DynamoDBv2.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APINotification.Models.Dynamo
{
    public class SurveyAnswer
    {
        [DynamoDBHashKey]
        public string Id { get; set; }

        [DynamoDBProperty]
        public string QuestionId { get; set; }

        [DynamoDBProperty]
        public string Answer { get; set; }

        [DynamoDBProperty]
        public bool IsDelete { get; set; }

        [DynamoDBProperty]
        public DateTime CreatedDate { get; set; }

        [DynamoDBProperty]
        public DateTime? ModifiedDate { get; set; }
    }
}
