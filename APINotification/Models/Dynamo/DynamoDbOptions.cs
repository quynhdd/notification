﻿using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APINotification.Models.Dynamo
{
    public class DynamoDbOptions
    {
        public string SurveyQuestion { get; set; }
        public string SurveyAnswer { get; set; }
        public string SurveyUser { get; set; }
        public string SurveyQuestionType { get; set; }
    }
}
