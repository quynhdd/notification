﻿using Amazon.DynamoDBv2.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APINotification.Models.Dynamo
{
    public interface IDynamoDbContext<T> : IDisposable where T : class
    {
        Task<T> GetByIdAsync(string id);
        Task SaveAsync(T item);
        Task DeleteByIdAsync(T item);
        Task<List<T>> GetAll();
        Task<List<T>> GetByAttribute(List<ScanCondition> scanConditions);
        Task SaveAsync(List<T> item);
    }
}
