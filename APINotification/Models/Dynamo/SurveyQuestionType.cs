﻿using Amazon.DynamoDBv2.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APINotification.Models.Dynamo
{
    public class SurveyQuestionType
    {
        [DynamoDBHashKey]
        public int QuestionType { get; set; }

        [DynamoDBProperty]
        public string QuestionTypeName { get; set; }
    }
}
