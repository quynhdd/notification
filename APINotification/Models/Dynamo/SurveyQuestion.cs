﻿using Amazon.DynamoDBv2.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APINotification.Models.Dynamo
{
    public class SurveyQuestion
    {
        [DynamoDBHashKey]
        public string Id { get; set; }

        [DynamoDBProperty]
        public int Type { get; set; }
        
        [DynamoDBProperty]
        public string Question { get; set; }

        [DynamoDBProperty]
        public DateTime StartDate { get; set; }

        [DynamoDBProperty]
        public DateTime EndDate { get; set; }

        [DynamoDBProperty]
        public string Image { get; set; }

        [DynamoDBProperty]
        public int UserType { get; set; }

        [DynamoDBProperty]
        public string NotiTitle { get; set; }

        [DynamoDBProperty]
        public string NotiContent { get; set; }

        [DynamoDBProperty]
        public bool IsDelete { get; set; }

        [DynamoDBProperty]
        public DateTime CreatedDate { get; set; }

        [DynamoDBProperty]
        public DateTime? ModifiedDate { get; set; }
    }
}
