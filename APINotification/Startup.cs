﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using APINotification.Extentions;
using APINotification.Models.Solution_30Shine;
using APINotification.Repository.Implement;
using APINotification.Repository.Interface;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.IdentityModel.Tokens;
using Swashbuckle.AspNetCore.Swagger;
using APINotification.Models.Dynamo;
using Amazon;
using Amazon.DynamoDBv2;

namespace APINotification
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
             .SetBasePath(env.ContentRootPath)
             .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
             .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
             .AddEnvironmentVariables();
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<Solution_30shineContext>(options =>
               options.UseSqlServer(Configuration.GetConnectionString("Solution_30shineContext")));
            //add interface
            services.AddTransient<INotificationManagementRepo, NotificationManagementRepo>();
            services.AddTransient<INotificationUsersRepo, NotificationUsersRepo>();
            services.AddSingleton<IPushNotice, PushNotice>();
            services.AddTransient<IStaffRepo, StaffRepo>();
            services.AddTransient<ISalonRepo, SalonRepo>();
            services.AddTransient<IStaffTypeRepo, StaffTypeRepo>();
            services.AddTransient<IBookingRepo, BookingRepo>();
            services.AddTransient<ICustomerRepo, CustomerRepo>();




            // AWS Options
            var awsOptions = Configuration.GetAWSOptions();
            Environment.SetEnvironmentVariable("AWS_ACCESS_KEY_ID", Configuration["AWS:AccessKey"]);
            Environment.SetEnvironmentVariable("AWS_SECRET_ACCESS_KEY", Configuration["AWS:SecretKey"]);
            Environment.SetEnvironmentVariable("AWS_REGION", Configuration["AWS:Region"]);

            services.AddDefaultAWSOptions(awsOptions);
            awsOptions.Region = RegionEndpoint.APSoutheast1;

            var client = awsOptions.CreateServiceClient<IAmazonDynamoDB>();

            var dynamoDbOptions = new DynamoDbOptions();
            ConfigurationBinder.Bind(Configuration.GetSection("DynamoDbTables"), dynamoDbOptions);

            // This is where the magic happens
            services.AddScoped<IDynamoDbContext<SurveyQuestion>>(provider => new DynamoDbContext<SurveyQuestion>(client, dynamoDbOptions.SurveyQuestion));
            services.AddScoped<IDynamoDbContext<SurveyAnswer>>(provider => new DynamoDbContext<SurveyAnswer>(client, dynamoDbOptions.SurveyAnswer));
            services.AddScoped<IDynamoDbContext<SurveyUser>>(provider => new DynamoDbContext<SurveyUser>(client, dynamoDbOptions.SurveyUser));
            services.AddScoped<IDynamoDbContext<SurveyQuestionType>>(provider => new DynamoDbContext<SurveyQuestionType>(client, dynamoDbOptions.SurveyQuestionType));




            //auth service
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Configuration["Jwt:Issuer"],
                        ValidAudience = Configuration["Jwt:Issuer"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]))
                    };
                });
            services.AddMvc();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    TermsOfService = "None",
                });
                c.IgnoreObsoleteActions();
                c.IgnoreObsoleteProperties();
                c.CustomSchemaIds((type) => type.FullName);
                try
                {
                    var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                    var xmlPath = Path.Combine(basePath, "APINotification.xml");
                    c.IncludeXmlComments(xmlPath);
                }
                catch { }
            });
            services.AddCors(options =>
            {
                options.AddPolicy("AnyOrigin", builder =>
                {
                    builder
                        .AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                });
            });
            services.AddDirectoryBrowser();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddFile("Logs/myapp-{Date}.txt");
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            // Enable use files
            app.UseFileServer();
            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseCors(builder =>
            {
                builder.AllowAnyHeader();
                builder.AllowAnyMethod();
                builder.AllowCredentials();
                builder.AllowAnyOrigin();
                //corsBuilder.WithOrigins("http://localhost:51060/"); // for a specific url.
            });
            //use auth
            app.UseAuthentication();
            app.UseMvc();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "API V2");
                c.InjectStylesheet("/swagger/ui/custome.css");
                c.InjectStylesheet("/lib/semantic-ui/dist/semantic.css");
                c.InjectJavaScript("/lib/semantic-ui/dist/semantic.js");
                c.InjectJavaScript("/swagger/authen/custome.js");
                c.InjectJavaScript("/swagger/authen/basic-auth.js");
                c.RoutePrefix = "notification";
            });
            app.UseCors("CorsPolicy");
            app.UseAuthentication();
        }
    }
}

