﻿using System;
using System.Text.RegularExpressions;

namespace APINotification.Utils
{
    public static class ValidateExtensions
    {
        public static bool IsValidPhone(string Phone)
        {
            try
            {
                if (string.IsNullOrEmpty(Phone))
                    return false;
                //var r = new Regex(@"^\(?([0-9]{3})\)?[-.●]?([0-9]{3})[-.●]?([0-9]{4})$");
                var r = new Regex(@"(09|08|01|02|[0|1|2|3|4|5|6|7|8|9])+([0-9]{8})\b");
                return r.IsMatch(Phone.Trim().Replace(", ",""));

            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool IsValidEmail(string email)
        {
            try
            {
                if (string.IsNullOrEmpty(email))
                    return false;
                var r = new Regex(@"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
                return r.IsMatch(email);
            }
            catch (FormatException)
            {
                return false;
            }
        }

        public static bool IsValidDatetime(string format, string datetime)
        {
            return DateTime.TryParseExact(datetime, format, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.NoCurrentDateDefault, out var dateTime);
        }
    }
}
