﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using APINotification.Models.CustomeModel;
using System.Net;
using System.IO;

namespace APINotification.Extentions
{
    public class PushNotice : IPushNotice
    {
        private readonly string domainPushNotice;
        private readonly string apiNoticeSlack;
        private bool IsPushSlack;
        private readonly string domainOnesignal;
        private readonly string apiOnesignal;
        private readonly IConfiguration Configuration;
        public PushNotice(IConfiguration configuration)
        {
            Configuration = configuration;
            //get api using
            domainPushNotice = Configuration.GetSection("APIsUsing:DomainPushNotice").Value;
            apiNoticeSlack = String.Concat(domainPushNotice, Configuration.GetSection("APIsUsing:ApiNoticeToSlack").Value);
            // get api onsignal
            domainOnesignal = Configuration.GetSection("APIOnesignal:Domain").Value;
            apiOnesignal = String.Concat(domainOnesignal, Configuration.GetSection("APIOnesignal:Api").Value);
            //get setting
            IsPushSlack = Boolean.Parse(Configuration.GetSection("AppSettings:IsPushSlack").Value);
        }

        /// <summary>
        /// Push lỗi tới ứng dụng slack
        /// </summary>
        /// <param name="className"></param>
        /// <param name="methodName"></param>
        /// <param name="messageError"></param>
        public void PushErrorToSlack(string className, string methodName, string messageError)
        {
            try
            {
                if (IsPushSlack)
                {
                    string uri = apiNoticeSlack;
                    var content = new
                    {
                        list_group_id = new[] { AppConstants.GROUP_SLACK },
                        type = AppConstants.TYPE_ERROR,
                        team = AppConstants.TEAM,
                        message = String.Format("Error: {0} \n __ClassName: {1} \n __MethodName: {2}", messageError, className, methodName),
                        module_name = AppConstants.MODULE_NAME
                    };
                    new System.Net.Http.HttpClient().PostAsync(uri,
                        new StringContent(JsonConvert.SerializeObject(content), Encoding.UTF8, "application/json"));
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Push thông tin tới ứng dụng slack
        /// </summary>
        /// <param name="message"></param>
        public void PushInfoToSlack(string message)
        {
            if (IsPushSlack)
            {
                string uri = apiNoticeSlack;
                var content = new
                {
                    list_group_id = new[] { AppConstants.GROUP_SLACK },
                    type = AppConstants.TYPE_INFO,
                    team = AppConstants.TEAM,
                    message = String.Format("Info: {0}", message),
                    module_name = AppConstants.MODULE_NAME
                };
                new System.Net.Http.HttpClient().PostAsync(uri,
                    new StringContent(JsonConvert.SerializeObject(content), Encoding.UTF8, "application/json"));
            }
        }

        public void PushNotiBrandNameVMG(string phone, string message)
        {
            try
            {
                var domain = Configuration.GetSection("APIsUsing:DomainBrandNameVMG").Value;
                var api = Configuration.GetSection("APIsUsing:ApiBrandNameVMG").Value;
                var data = new
                {
                    phone_number = phone,
                    message = message
                };
                 new HttpClient().PostAsync(domain + api, new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json"));
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// Hàm push thông báo cho app khi cần. 
        /// </summary>
        /// <param name="apiKey">key bảo mật của app</param>
        /// <param name="appId">id app trên onesignal</param>
        /// <param name="filter">bộ lọc sẽ bắn cho app client nào</param>
        /// <param name="objData">data mà app sẽ nhận được khi bắn noti 
        ///                       VD: var button = new object [] { };// new object[] { new { id = "id1", text = "button1" }, new { id = "id2", text = "button2" } };
        /// </param>
        /// <param name="button">hiện nút bấm khi có noti. trên thông báo của app</param>
        /// <param name="pictureUrl">Đường dẫn ảnh, hiện icon của noti trên thông báo của app</param>
        /// <param name="isAll">có phải bắn tất cả các app client. == true là tất. = false là không</param>
        /// <returns></returns>
        public void PushStaffCustomer(string apiKey, string appId, object[] filter, string messageName, string messageTitle, string pictureUrl = "", bool isAll = false, object objData = null, object[] button = null)
        {
            try
            {
                var request = WebRequest.Create(apiOnesignal) as HttpWebRequest;
                request.KeepAlive = true;
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";
                request.Headers.Add("authorization", "Basic " + apiKey);
                var obj = new
                {
                    app_id = appId,
                    contents = new { en = messageTitle },
                    headings = new { en = messageName },
                    ios_badgeType = "Increase",
                    ios_badgeCount = 1,
                    included_segments = isAll ? new string[] { "All" } : new string[] { },
                    filters = isAll ? new object[] { } : filter,
                    data = objData ?? new { },
                    buttons = button ?? new object[] { },
                    ios_attachments = new { id = pictureUrl }
                };
                var param = JsonConvert.SerializeObject(obj);
                byte[] byteArray = Encoding.UTF8.GetBytes(param);
                string responseContent = null;
                try
                {
                    using (var writer = request.GetRequestStream())
                    {
                        writer.Write(byteArray, 0, byteArray.Length);
                    }

                    using (var response = request.GetResponse() as HttpWebResponse)
                    {
                        using (var reader = new StreamReader(response.GetResponseStream()))
                        {
                            responseContent = reader.ReadToEnd();
                        }
                    }
                }
                catch (WebException e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                    System.Diagnostics.Debug.WriteLine(new StreamReader(e.Response.GetResponseStream()).ReadToEnd());
                    throw e;
                }

                System.Diagnostics.Debug.WriteLine(responseContent);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Hàm push noti rating app customer
        /// </summary>
        /// <param name="apiKey">key bảo mật của app</param>
        /// <param name="appId">id app trên onesignal</param>
        /// <param name="filter">bộ lọc sẽ bắn cho app client nào</param>
        /// <param name="objData">data mà app sẽ nhận được khi bắn noti 
        /// VD: var button = new object [] { };// new object[] { new { id = "id1", text = "button1" }, new { id = "id2", text = "button2" } };
        /// </param>
        /// <param name="button">hiện nút bấm khi có noti. trên thông báo của app</param>
        /// <param name="pictureUrl">Đường dẫn ảnh, hiện icon của noti trên thông báo của app</param>
        /// <param name="isAll">có phải bắn tất cả các app client. == true là tất. = false là không</param>
        /// <returns></returns>
        public string PushNotiRatingAppCustomer(string apiKey, string appId, object[] filter, string messageName, string messageTitle, string pictureUrl = "", bool isAll = false, object objData = null, object[] button = null)
        {
            string statusCode = "";
            var request = WebRequest.Create(apiOnesignal) as HttpWebRequest;
            request.KeepAlive = true;
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";
            request.Headers.Add("authorization", "Basic " + apiKey);
            var obj = new
            {
                app_id = appId,
                contents = new { en = messageTitle },
                headings = new { en = messageName },
                ios_badgeType = "Increase",
                ios_badgeCount = 1,
                included_segments = isAll ? new string[] { "All" } : new string[] { },
                filters = isAll ? new object[] { } : filter,
                data = objData ?? new { },
                buttons = button ?? new object[] { },
                ios_attachments = new { id = pictureUrl }
            };
            var param = JsonConvert.SerializeObject(obj);
            byte[] byteArray = Encoding.UTF8.GetBytes(param);
            string responseContent = null;
            try
            {
                using (var writer = request.GetRequestStream())
                {
                    writer.Write(byteArray, 0, byteArray.Length);
                }

                using (var response = request.GetResponse() as HttpWebResponse)
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        responseContent = reader.ReadToEnd();
                    }
                    statusCode = response.StatusCode.ToString();
                }
            }
            catch (WebException e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                System.Diagnostics.Debug.WriteLine(new StreamReader(e.Response.GetResponseStream()).ReadToEnd());
                throw e;
            }
            //
            return statusCode;
        }

        /// <summary>
        /// push notification use onesignal
        /// </summary>
        public void PushNotificationOnesignal(List<int> listUser, int type, bool isAllUser, string messageName, string messageTitle, string messageImage, object data = null)
        {
            try
            {
                if (listUser == null || type <= 0)
                {
                    throw new Exception();
                }

                var appId = "";
                var apiKey = "";
                switch (type)
                {
                    // = 1 thông báo cho app staff
                    case 1: appId = AppConstants.ID_APPSTAFF; apiKey = AppConstants.API_KEY_APPSTAFF; break;
                    // = 2: thông báo cho app customer
                    case 2: appId = AppConstants.ID_APPCUSTOMER; apiKey = AppConstants.API_KEY_APPCUSTOMER; break;
                }
                if (isAllUser)
                {
                    // call onesignal bắn all user
                    PushStaffCustomer(apiKey, appId, new object[] { }, messageName, messageTitle, messageImage, isAllUser, data);
                    return;
                }
                // Số lượng user được phép bắn thông báo trong 1 lần.
                var numberFilteruser = 100;
                // số lượng user < 100
                if (listUser.Count <= numberFilteruser && listUser.Count > 0)
                {
                    var filters = new object[listUser.Count * 2 - 1];
                    for (var i = 0; i < listUser.Count; i++)
                    {
                        filters[i * 2] = new { field = "tag", key = "user-id", relation = "=", value = listUser[i] };
                        if (i < listUser.Count - 1)
                        {
                            filters[i * 2 + 1] = new { @operator = "OR" };
                        }
                    }
                    //call xử lý onesignal
                    PushStaffCustomer(apiKey, appId, filters, messageName, messageTitle, messageImage, isAllUser, data);
                }
                // số lượng user lớn hơn 100
                else if (listUser.Count > numberFilteruser)
                {
                    var filters = new object[numberFilteruser * 2 - 1];
                    var dem = 0;
                    for (var i = 0; i < listUser.Count; i++)
                    {
                        filters[dem * 2] = new { field = "tag", key = "user-id", relation = "=", value = listUser[i] };
                        if (dem * 2 < filters.Count() - 1)
                        {
                            filters[dem * 2 + 1] = new { @operator = "OR" };
                        }
                        dem++;
                        if (dem == ((filters.Count() + 1) / 2))
                        {
                            dem = 0;
                            // Check trường hợp object cuối cùng của filter nếu là "OR" call tất cả noti.
                            var filterTemp = filters.ToList();
                            if (filterTemp.LastOrDefault().ToString() == "{ operator = OR }")
                            {
                                filterTemp.RemoveAt(filterTemp.Count - 1);
                            }
                            PushStaffCustomer(apiKey, appId, filterTemp.ToArray(), messageName, messageTitle, messageImage, isAllUser, data);
                            if (listUser.Count - i < numberFilteruser)
                            {
                                filters = new object[(listUser.Count - i) * 2 - 2];
                            }
                            else
                            {
                                filters = new object[numberFilteruser * 2 - 1];
                            }
                        }
                    }
                }

            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
