﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace APINotification.Utils
{
    public static class StringExtension
    {
        static readonly CultureInfo culture = new CultureInfo("vi-VN");

        public static string EmptyIfNull(this object value)
        {
            if (value == null)
            {
                return "";
            }
            else
                return value.ToString();
        }

        public static int ZeroIfNull(this object value)
        {
            if (value == null)
            {
                return 0;
            }
            else
                return (int)value;
        }

        /// <summary>
        /// Count words with Regex.
        /// </summary>
        public static int CountWords(string s)
        {
            MatchCollection collection = Regex.Matches(s, @"[\S]+");
            return collection.Count;
        }

        public static string GetWords(string input, int take)
        {
            string[] words = GetWords(input);
            int wordsLength = words.Length;
            string result = "";
            if (wordsLength >= take)
                for (int i = 0; i < take; ++i)
                {
                    result += words[i].ToString() + " ";
                }
            else
                result = input;
            return result;
        }


        public static string GetWords(string input, int from, int take)
        {
            string[] words = GetWords(input);
            int wordsLength = words.Length;
            string result = "";
            if (wordsLength >= take)
                for (int i = from; i < take; ++i)
                {
                    result += words[i].ToString() + " ";
                }
            else
                result = input;
            return result;
        }


        static string[] GetWords(string input)
        {
            MatchCollection matches = Regex.Matches(input, @"\b[\w']*\b");

            var words = from m in matches.Cast<Match>()
                        where !string.IsNullOrEmpty(m.Value)
                        select TrimSuffix(m.Value);
            return words.ToArray();
        }

        public static string StripHTML(string input)
        {
            return Regex.Replace(input, "<.*?>", String.Empty);
        }

        private static string TrimSuffix(string word)
        {
            int apostropheLocation = word.IndexOf('\'');
            if (apostropheLocation != -1)
            {
                word = word.Substring(0, apostropheLocation);
            }
            return word;
        }


        public static string FirstCharToUpper(string input)
        {
            if (String.IsNullOrEmpty(input))
                throw new ArgumentException("string empty!");
            return input.First().ToString().ToUpper() + input.Substring(1);
        }

        public static string UppercaseFirstEach(string s)
        {
            char[] a = s.ToLower().ToCharArray();

            for (int i = 0; i < a.Length; i++)
            {
                a[i] = i == 0 || a[i - 1] == ' ' ? char.ToUpper(a[i]) : a[i];

            }
            return new string(a);
        }

        public static void AddRangeDateTime(string s, out DateTime startDate, out DateTime endDate)
        {
            startDate = Convert.ToDateTime(s, culture);
            endDate = startDate.AddDays(1);
        }

        public static List<DateTime> ConvertToDateTime(params string[] multiString)
        {
            List<DateTime> list = new List<DateTime>();
            foreach (var item in multiString)
            {
                DateTime dateTime = Convert.ToDateTime(item, culture);
                list.Add(dateTime);
            }
            return list;
        }

        public static bool isStringContainAllDigit(string s)
        {
            if (s != null)
            {
                foreach (char c in s)
                {
                    if (c > '9' || c < '0')
                    {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }

        public static string GenUuid()
        {
            return Guid.NewGuid().ToString();
        }

        public static IEnumerable<int> ConvertString(this string data)
        {
            return data.Split(",").Select(a => int.TryParse(a.Trim(), out var integer) ? integer : 0);
        }

    }
}
