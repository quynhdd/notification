﻿using System;

namespace APINotification.Utils
{
    public static class ReflectionExtension
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="objInput"></param>
        /// <param name="objOutput"></param>
        /// <returns></returns>
        public static Object CopyObjectValue(Object objInput, Object objOutput)
        {
            var propInfo = objInput.GetType().GetProperties();
            foreach (var item in propInfo)
            {
                objOutput.GetType().GetProperty(item.Name).SetValue(objOutput, item.GetValue(objInput, null), null);
            }
            return objOutput;
        }
    }
}
