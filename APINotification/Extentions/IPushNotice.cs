﻿using APINotification.Models.CustomeModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APINotification.Extentions
{
    public interface IPushNotice
    {
        void PushErrorToSlack(string className, string methodName, string messageError);
        void PushInfoToSlack(string message);
        //int PushNotiBrandNameVMG(string phone, string message);
        void PushStaffCustomer(string apiKey, string appId, object[] filter, string messageName, string messageTitle, string pictureUrl="", bool isAll = false, object objData = null, object[] button = null);
        void PushNotificationOnesignal(List<int> listUser, int type, bool isAllUser, string messageName, string messageTitle, string messageImage, object data = null);
        string PushNotiRatingAppCustomer(string apiKey, string appId, object[] filter, string messageName, string messageTitle, string pictureUrl = "", bool isAll = false, object objData = null, object[] button = null);
    }
}
