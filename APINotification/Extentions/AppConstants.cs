﻿namespace APINotification.Extentions
{
    public class AppConstants
    {
        public static string APP_ID = "myApp001";
        public static string API_ID = "myAPI001";
        public readonly string[] GROUP_OR_USERNAME = { "sys", "duycs", "admin", "slm", "gsm", "gs" };
        public readonly string[] PERMISSTIONS_BASIC = { "fullControl", "modify", "readExcute", "read", "listContents", "write", "special" };
        public readonly string[] RESOURCE_TABLE = { "table1", "table2", "table3" };
        public readonly string[] RESOURCE_API = { "api1", "api2", "api3" };
        public readonly string[] ACTION = { "add", "remove", "edit", "view" };
        public const string ACTION_READ = "read";
        public const string ACTION_WRITE = "write";
        public const string ACTION_EDIT = "edit";
        public const string ACTION_VIEW = "view";
        public const string ACTION_DEL = "del";


        //notify
        public const bool IS_LOG = false;
        public const string GROUP_SLACK = "CG725SAKS";
        public const string API_PUSH_NOTIC_TO_SLACK = "https://api-push-notic.30shine.com/api/pushNotice/slack";
        public const string TEAM = "api";
        public const string MODULE_NAME = "30Shine\\API-Notification";
        public const string TYPE_ERROR = "Error";
        public const string TYPE_INFO = "Info";
        public const string TYPE_WARNING = "Warning";
        //App push notification use onesignal
        public readonly static string API_KEY_APPCUSTOMER = "NjM3ZGZmNzUtMGQ3MC00OGRjLTljM2MtYzhkMzEwMTQzOGMx";
        public readonly static string ID_APPCUSTOMER = "88f5d0e2-2aaa-49bc-bd2b-f88cb226027c";
        public readonly static string API_KEY_APPSTAFF = "NmMwODI2OTUtZDYwNC00Y2I1LTg2MTctYjg0ODMyZDE5NzM3";//"ZjY4N2M5OGQtZDU5My00ZjRjLTk0MzctNGJkMWJkNDJlYjc1";
        public readonly static string ID_APPSTAFF = "f6fb7a91-1284-444d-97f9-2f3c939cf7e5";//"3c378db6-23ce-41b6-8760-15ffe777dd78";
        //date
        public const string DATE_FORMAT = "dd-MM-yyyy";


        //
        public const int SALON_NEAR_SLOT = 5;

        /// <summary>
        /// mỹ phẩm sau khi bán
        /// </summary>
        public const int COSMETIC_SELL = 1;

        public const int SURVEY_QUESTION_MULTIPLE_CHOICE = 3;
    }
}
