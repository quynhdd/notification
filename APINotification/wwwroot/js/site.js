﻿console.log("Go start!");
$(function () {
    let message = window.getUrlParameter("message");
    if (message) {
        window.showToast("msg", message);
    }
});

//Check Jquery
(function (factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else if (typeof module === 'object' && module.exports) {
        // Node/CommonJS
        module.exports = function (root, jQuery) {
            if (jQuery === undefined) {
                if (typeof window !== 'undefined') {
                    console.log("jquery require!");
                    jQuery = require('jquery');
                }
                else {
                    console.log("jquery require!");
                    jQuery = require('jquery')(root);
                }
            }
            factory(jQuery);
            return jQuery;
        };
    } else {
        // Browser globals
        factory(jQuery);
    }
});


// get params in url
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};


function showToast(type, message) {
    let title = "";
    console.log(type + "|" + message);
    switch (type) {
        case "msg":
            title = "Ok!";
            iziToast.success({
                title: title,
                message: message,
            });
            break;
        case "err":
            title = "Error";
            iziToast.error({
                title: title,
                message: message,
            });
            break;
        case "war":
            title = "Oop!";
            iziToast.warning({
                title: title,
                message: message,
            });
            break;
        default:
            title = "Ok!";
            iziToast.message({
                title: title,
                message: message,
            });
    }

}